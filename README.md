Altscan checks for missing `alt` attributes in images in Drupal Field API data.

Alt text is required on all images that are not "purely decorative" in order for
a site to meet WCAG 2.0 level A (the lowest/basic level) compliance. WCAG is
also currently structured so that a site's overall rating is only set at the
level of the lowest scoring page within that site. The WCAG recommendation is
that even images that are "purely decorative" should be provided with NULL `alt`
attributes rather than completely ommitting the `alt` attribute altogether.

In many countries there are legal requirements for organisations to meet certain
accessibility standards, and "alt text" for images is a common and basic
requirement. The current trend is clearly towards increasing regulation from
governments requiring organisations implement accessibility standards, whether
WCAG 2.0 or some other similar specification.

Even without governments cracking the whip, site owners should _want_ to
implement alt text consistently as it broadens their audience, improves SEO and
improves overall site usability.

Unfortunately, while it is relatively easy to ensure alt text is present in
markup built through Drupal's render API by web professionals, it is much harder
to audit and ensure alt text is present on all images in content across an
entire site.

This module is conceptually very similar to the Link Checker module, it will
scan field data and provide specialized reports to help you ensure minimum
standards for your site's content. Unlike Link Checker, we have no need to
periodically re-scan content as the existence of an alt attribute on an image
should be static until the field data is next saved.

Technically this module addresses two primary use-cases:

- Images uploaded through the ImageField module, where the alt text is saved in
  a structured, predictable way to the database.
- Text fields with an associated input format that we expect to have run through
  check_markup() for display. We scan the _filtered_ text for `img` tags with
  missing `alt` attributes.

The module exposes `hook_altscan_field_type()` and
`hook_altscan_field_type_alter()` to allow other modules to extend and modify
the way the scanning of field data.

This module does not check the markup of fully rendered pages, there are other
tools available for in-depth, holistic page-by-page accessibility analysis.
Altscan is intended to give you a report only on _field data_ in your database
and so is an analysis of your _field content only_, not your structure, blocks,
theme, custom logic or anything else.

## Current state of Altscan

This module was written in a weekend to help meet deliverables for a specific
project with a specific set of accessibility requirements. It has been written
in a way that should make it relatively easy to extend/improve in the future
without needing major refactors, but there are a few obvious things that could
make the module even more useful:

- Batch processing for the image scanner for large databases with many Entities.
- More fine grained permissions, reports tailored to an individual user's
  content.
- Improved integration with contrib field types that work with images.
- Advanced integration with common contrib Entity types, such as linking to the
  parent entity of a field collection entity in the reports.
- Scanning in places beyond Field API data.
- Scanning for other important accessibility attributes such as `title` or aria
  roles.
- Warnings/form validation directly on Entity edit forms highlighting invalid
  images at the point the Entity is saved.
- Support for non-text content beyond images.
- Configuration options to customise the module's behaviour.
- An automated test suite.

## Collaborating on this project

- I welcome suggestions that come with _working patches_.
- I welcome anyone who wants to co-maintain the project with me.
- I welcome anyone who wants to commission (pay for at Australian market rates)
  additional functionality for this module so that it better fits their
  own project's needs.
- I welcome clear bug reports.
- I am unlikely to work on functionality or feature requests that are missing or
  only have incomplete patches.

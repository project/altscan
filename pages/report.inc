<?php
/**
 * @file
 * Report page for altscan.
 */

function altscan_report_page_rescan() {
  $form = array();
  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => 'Re-scan content for images',
  );
  return $form;
}

function altscan_report_page_rescan_submit(&$form, &$form_state) {
  altscan_scan_all();
}

/**
 * Page callback for altscan report.
 */
function altscan_report_page($filter = 'all') {
  $build = array();

  $build['rescan_form'] = drupal_get_form('altscan_report_page_rescan');

  $help = '';
  switch ($filter) {
    case 'null-alt':
      $help = t('WCAG 2.0 level A compliance requires that images with null alt attributes are "purely decorative". If you have images that represent "content", you should add a description of the image to the alt attribute.');
      break;

    case 'has-alt':
      $help = t('WCAG 2.0 level A compliance requires that "purely decorative" images have null alt attributes as descriptions of purely aesthetic page elements are distracting. If you have images that are not "content" with descriptions (such as "drop shadow" or "red gradient"), you should remove the description.');
      break;

    case 'missing':
    default:
      $help = t('WCAG 2.0 level A compliance requires that no images are completely missing alt attributes. You should review any images below and ensure they have alt attributes set.');
      break;
  }
  $build['help'] = array('#markup' => "<p>$help</p>");

  drupal_add_css('#altscan-report img { max-width: 20vw; height: auto; } ', 'inline');
  $header = array(
    array(
      'data' => t('Image'),
    ),
    array(
      'data' => t('Entity information'),
      'field' => 'a.entity_id',
    ),
    array(
      'data' => t('Image information'),
    ),
  );

  $per_page = 100;
  $query = db_select(ALTSCAN_TABLE, 'a')
    ->fields('a', array('entity_type', 'bundle', 'entity_id', 'language', 'field_name', 'image_html', 'alt', 'has_alt'))
    ->extend('TableSort')
    ->extend('PagerDefault')
    ->orderBy('entity_id', 'ASC')
    ->orderByHeader($header)
    ->limit($per_page);

  if ($filter === 'missing') {
    $query->condition('a.has_alt', 0, '=');
  }

  if ($filter === 'null-alt') {
    $query
      ->condition('a.has_alt', 1, '=')
      ->condition('a.alt', '', '=');
  }

  if ($filter === 'has-alt') {
    $query
      ->condition('a.has_alt', 1, '=')
      ->condition('a.alt', '', '!=');
  }

  $result = $query->execute();

  $rows = array();
  while($data = $result->fetchObject()){
    $entity = entity_load($data->entity_type, array($data->entity_id));
    $entity = reset($entity);

    $uri = entity_uri($data->entity_type, $entity);

    $entity_info = array();
    if (!empty($entity->title)) {
      $entity_info['title'] = array(
        '#prefix' => 'Title: ',
        '#markup' => !empty($uri) ? l($entity->title, $uri['path'], $uri['options']) : $entity->title,
      );
    }

    if (!empty($data->entity_type)) {
      $entity_info['type'] = array(
        '#prefix' => 'Entity type: ',
        '#markup' => $data->entity_type,
      );
    }

    if (!empty($data->entity_id)) {
      $entity_info['id'] = array(
        '#prefix' => 'Entity ID: ',
        '#markup' => $data->entity_id,
      );
    }

    if (!empty($data->bundle)) {
      $entity_info['bundle'] = array(
        '#prefix' => 'Entity bundle: ',
        '#markup' => $data->bundle,
      );
    }

    if (!empty($data->field_name)) {
      $entity_info['field_name'] = array(
        '#prefix' => 'Field name: ',
        '#markup' => $data->field_name,
      );
    }

    if (!empty($data->language)) {
      $entity_info['language'] = array(
        '#prefix' => 'Language code: ',
        '#markup' => $data->language,
      );
    }

    $image_info = array();

    $image_info['html'] = array(
      '#prefix' => 'Image HTML: ',
      '#markup' => check_plain($data->image_html),
    );

    $image_info['Has alt text'] = array(
      '#prefix' => 'Image has an alt attribute: ',
      '#markup' => (bool) $data->has_alt ? 'Yes' : 'No',
    );

    $image_info['alt text'] = array(
      '#prefix' => 'Image alt text: ',
      '#markup' => $data->alt,
    );

    $infos = array(&$entity_info, &$image_info);
    foreach ($infos as &$info) {
      foreach ($info as &$text) {
        $text['#prefix'] = '<strong>' . $text['#prefix'] . '</strong>';
        $text['#suffix'] = '<br />';
      }
    }

    // Fill the table rows
    $rows[] = array(
      $data->image_html,
      drupal_render($entity_info),
      drupal_render($image_info),
    );
  }

  $build['table'] = array(
    '#theme' => 'table',
    '#header' => $header,
    '#rows' => $rows,
    '#attributes' => array('id' => array('altscan-report')),
    '#sticky' => TRUE,
    '#caption' => '',
    '#colgroups' => array(),
    '#empty' => t("There are no scanned images for this report."),
  );

  $build['pager'] = array('#theme' => 'pager');

  return $build;
}
